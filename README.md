# Cordova plugin for iOS 9

`cordova plugin add https://repogit/cordova-ios9.git`

## Security

It will add the following part to the `*-Info.plist` file during build process:

    <key>NSAppTransportSecurity</key> 
    <dict>
      <key>NSAllowsArbitraryLoads</key> <true/> 
    </dict>

## ITMS-90475 issue

It will add the following part to the `*-Info.plist` file during build process:

    <key>UIRequiresFullScreen</key> 
    <string>YES</string>
    
This will fix ERROR ITMS-90475: "Invalid Bundle. iPad Multitasking support requires launch story board in bundle..."
